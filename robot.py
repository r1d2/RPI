#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#

import src.r1d2 as r1d2
import os
import sys

#sys.path.append("/home/arnaud/files/programmation/python/robot/R1D2/src/")

if __name__ == '__main__':
    try:
        print(os.getcwd())
        r = r1d2.Robot('./')
        r.launch('r1d2.ini')
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
