#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser



class ConfHandler:
    def __init__(self, path_ini):
        self.p = path_ini
    def config_loader(self):
        config = configparser.ConfigParser()
        config.read(self.p)
        return config

    @staticmethod
    def configsectionmap(config, section):
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
                if dict1[option] == -1:
                    print("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def write_config(self):
        cfgfile = open(self.p, 'w')
        config = configparser.ConfigParser()
        config.add_section('Config')
        config.set('Config', 'window', 'true')

        config.set('Config', 'debug', 'DEBUG')

        config.add_section('I2C')
        config.set('Config', 'arduino_i2c', 'false')
        config.set('Config', 'arduino_addr', '0x12')
        config.set('Config', 'arduino_frequency', '15')

        config.add_section('Camera')
        config.set('Camera', 'res_h', 480)
        config.set('Camera', 'res_w', 640)
        config.set('Camera', 'movie', 'false')

        config.add_section('Faces')
        config.set('Faces', 'faceset', 'lbpcascade_frontalface.xml')
        config.set('Faces', 'fistset', 'fist.xml')
        config.set('Faces', 'otherset', 'palm.xml')

        config.add_section('XMPP')
        config.set('XMPP', 'XMPP', 'true')
        config.set('XMPP', 'robot_jid', 'robot@example.com')
        config.set('XMPP', 'password', 'pass')
        config.set('XMPP', 'to_jid', 'me@example.com')
        config.set('XMPP', 'xmpp.register_plugin', "xep_0030 xep_0199 ", "xep_0004", "xep_0045", "xep_0050")

        config.write(cfgfile)
        cfgfile.close()

    def read_config(self):
        section = 'Config'
        c = self.config_loader()
        window = c.getboolean(section, 'window')
        debug = c.get(section, 'debug')
        print(section, window, debug)

        section = 'I2C'
        arduino_i2c = c.getboolean(section, 'arduino_i2c')
        arduino_addr = c.get(section, 'arduino_addr')
        arduino_freq = c.getint(section, 'frequency')
        print(section, arduino_i2c, arduino_addr, arduino_freq)

        section = 'Camera'
        res_h = c.getint(section, 'res_h')
        res_w = c.getint(section, 'res_w')
        movie = c.getboolean(section, 'movie')
        print(section, res_h, res_w, movie)
        section = 'Faces'
        faceset = c.get(section, 'faceset')
        fistset = c.get(section, 'fistset')
        otherset = c.get(section, 'otherset')
        print(section, faceset, fistset, otherset)
        section = 'XMPP'
        xmpp_enable = c.getboolean(section, 'xmpp')
        robot_jid = c.get(section, 'robot_jid')
        passwd = c.get(section, 'password')
        to_jid = c.get(section, 'to_jid')
        room =  c.get(section, 'muc_room')
        nick = c.get(section, 'muc_nick')
        xmpp_register_plugin = c.get(section, 'xmpp.register_plugin')
        print(section, xmpp_enable, robot_jid, to_jid, passwd, xmpp_register_plugin)
        section = 'LCD'
        lcd_enable = c.getboolean(section, 'lcd')
        print(section, lcd_enable)
        r = [[window, debug], [res_h, res_w, movie], [faceset, fistset, otherset], [xmpp_enable,
             robot_jid, to_jid , passwd, room, nick, xmpp_register_plugin], [lcd_enable],
             [arduino_i2c, arduino_addr, arduino_freq]]
        return r


if __name__ == '__main__':
    # Setup the command line arguments.
    path = '../r1d2.ini'
    s = ConfHandler(path)
    s.write_config()
    #s.read_config()
