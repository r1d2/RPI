##!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#



import random
import numpy as np
import matplotlib.pyplot as plt

STD = 20


class Generate:
    def __init__(self):
        self.obstacles = np.array([0, 0, 0, 0])

    @staticmethod
    def new_pt(r, std):
        # generate the position of a point but add some errors
        random.seed()
        s = random.uniform(-std, std)
        res = r + s
        return res

    def new_coord(self, mes):
        mes_new = []
        for i in mes:
            m = self.new_pt(i, STD)
            mes_new.append(m)
        return mes_new

    @staticmethod
    def size_conservation(mesure):
        total_h = mesure[2] + mesure[3]
        total_w = mesure[0] + mesure[1]
        return [total_w, total_h]

    def test_mesures(self, mesure):
        t = mesure[3:6]
        a = any(n < 0 for n in t)  # True if nevative value
        return not a

    def process_loop(self, dim_room, mesure):
        dim_room2 = self.size_conservation(mesure)
        if dim_room2[0] > dim_room[0] or dim_room2[1] > dim_room[1]:
            dim_room = dim_room2
        if self.test_mesures(mesure):
            mesure_append = [mesure[0], mesure[1], mesure[2], mesure[3]]
            self.obstacles = np.vstack((self.obstacles, mesure_append))
        return [dim_room]

    def robot(self):

        run = True
        item = 0
        mesure = [-1,-1,-1,-1]
        magn = [1,0,0]
        acc = [0,0,0]
        mesure = [0, magn, acc, 300, 20, 200, 10]  # au début, 4 senseurs de distance: gauche droite avant, arrière
        self.obstacles = np.vstack((self.obstacles, mesure))
        dim_room = self.size_conservation(mesure)
        while self.test_mesures(mesure):
            item += 1
            mesure[0] = item
            mesure[5] -= 30  # Il faudra un senseur a effet hall
            mesure[6] += 30
            #mesure = self.new_coord(mesure)
            self.process_loop(dim_room, mesure)
#bidouille
        magn = [0,1,0]
        mesure = [item , magn, acc, 300, 20,20, 190]
        while self.test_mesures(mesure):
             item += 1
             mesure[0] = item
             mesure[3] -= 30  # Il faudra un senseur a effet hall
             mesure[4] += 30
             self.process_loop(dim_room, mesure)






        self.obstacles = np.delete(self.obstacles, 0, 0)
        print self.obstacles
        print dim_room

        self.graph(dim_room, self.obstacles)

    def graph(self, dim_room, obstacles):

        size = obstacles.shape
        size = size[0]
        left_obs = -obstacles[:, 0]  # np.zeros(size)
        right_obs = obstacles[:, 1]  # + obstacles[:,0]
        up_obs = obstacles[:, 2]  # np.zeros(size)
        down_obs = -obstacles[:, 3]  # + obstacles[:,3]

        x = dim_room[0] - obstacles[:, 0]  - obstacles[:, 1]
        y = dim_room[1] - obstacles[:, 3]

        x_alt = obstacles[:, 0]
        y_alt = obstacles[:, 0]
        real_x = (x + x_alt) / 2.0
        real_y = (y + y_alt) / 2.0

        path = np.column_stack((real_x, real_y))

        fig, ax = plt.subplots()
        ax.scatter(x, y, c='y', alpha=0.5)
        ax.scatter(left_obs, up_obs, c='r', alpha=0.5)
        ax.scatter(right_obs, up_obs, c='b', alpha=0.5)
        ax.set_xlabel(r'W', fontsize=20)
        ax.set_ylabel(r'H', fontsize=20)
        ax.set_title('Map of your living room')
        ax.grid(True)
        fig.tight_layout()
        plt.show()


if __name__ == '__main__':
    # Setup the command line arguments.
    r = Generate()
    r.robot()
