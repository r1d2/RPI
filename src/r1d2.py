#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#

from multiprocessing import Process, Manager
import sys
import logging
import os
import time
import linecache
import glob


from .addons import xmpp
from .addons import faces
from .addons import lcd
from .addons import i2c
from . import config
# from . import sercom
from . import misc

logging.basicConfig()

# TODO: Tread to isolate the loop. Other Tread can stop it (e.g. a button is pressed)
log = logging.getLogger('root')


def file_test(path, t):
    if os.path.exists(path):
        return True
    else:
        m = path + ' does not exist \nPlease create ' + path + ' ' + t + ' in the robot.py directory.'
        logging.error(m)


class Robot:
    def __init__(self, root):
        self.root = root
        self.WINDOWS = False
        self.arduino_i2c = False
        self.XMPP = False
        self.lcd_enabled = False
        self.traget_detected = 0  # tracking mode or not
        self.checkpoint = 1
        self.prevac = 0
        self.facedetect = 0
        self.shared_vars = False
        self.lcd_instance = None
        self.arduino_addr = None

    def handle_exception(self):
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        m = 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
        log.error(m)

    def modules(self, conf):
        if conf[0][0]:
            # X11 windows can be opened
            self.WINDOWS = True
        if conf[3][0]:
            # XMPP communications enabled
            self.XMPP = True
        if conf[4][0]:
            # LCD Char plate enabled
            self.lcd_enabled = True
        if conf[5][0]:
            # I2C communication enabled
            self.arduino_i2c = True

    def launch(self, path='r1d2.ini'):
        m = misc.Misc()
        self.shared_vars = misc.Share()
        colors = misc.Colors()
        colors = vars(colors)
        manager = Manager()
        d_lcd = manager.dict()
        d_xmpp = manager.dict()
        d_mode = manager.dict()
        d_detect = manager.dict()
        d_i2c = manager.dict()

        # set all variables to false
        self.shared_vars.set_vars()
        # set all dictionary entries for xmpp to false
        v_share = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'send_dict': '0', 'roger': '0'}
        d_xmpp = self.update_manager_dict(d_xmpp, v_share)
        mode_skeleton = vars(self.shared_vars)
        d_mode = self.update_manager_dict(d_mode, mode_skeleton)
        conf = None
        try:
            path = self.root + path
            file_test(path, 'file')
            c = config.ConfHandler(path)
            conf = c.read_config()
            # DEBUG, ERROR, WARNING ETC
            log = m.setup_custom_logger('root', conf[0][1])

            self.modules(conf)
            self.shared_vars.set_listening_vars()

            if self.lcd_enabled:
                dict_lcd_skeleton = {'msg': ' ', 'color': ' ', 'menu': ' '}
                d_lcd = self.update_manager_dict(d_lcd, dict_lcd_skeleton)
                proc_lcd = Process(target=lcd.menu_r1d2.lcd_start, args=(d_lcd, d_mode, d_xmpp, mode_skeleton))
                proc_lcd.start()

            if self.XMPP:
                proc_xmpp = Process(target=xmpp.xmpp.init_xmpp,
                                    args=(d_xmpp, d_mode, d_lcd, mode_skeleton, conf[3][1], conf[3][2],
                                          conf[3][3], conf[3][4], conf[3][5], conf[3][6],))
                proc_xmpp.start()

            if self.arduino_i2c:
                self.arduino_addr = int(conf[5][1], 16)
                frequency = conf[5][2]  # update data every 60 seconds
                setting_file = "RTIMULib"
                proc_i2c = Process(target=i2c.i2c.init_i2c,
                                   args=(d_i2c, self.arduino_addr, frequency,setting_file, ))
                proc_i2c.start()

        except Exception:
            self.handle_exception()

        window = conf[0][0]
        faces_path = self.root + "known_faces"
        file_test(faces_path, 'directory')
        scale = 1
        interval = 0.1
        camera_num = 0
        resh = conf[1][0]
        resw = conf[1][1]
        movie = conf[1][2]
        path = self.root + "/cascades/cascades-enabled/"
        file_test(path, 'directory')
        cascpath = []
        files = glob.glob(self.root + "cascades/cascades-enabled/*.xml")
        for f in files:
            cascpath.append(f)
        capture_session = False
        while 1:

            print('###############################')
            for v in ('shared_vars_from_lcd', 'shared_vars_from_xmpp', 'frame_capture', 'video_mode', 'follow_mode',
                      'mapping_mode',
                      'facedetect_mode', 'sign_tracking', 'sign_phony'):
                if d_mode[v] is not False:
                    try:
                        print(v + ' ' + str(d_mode[v]))
                    except KeyError:
                        print("Key error d_mode")

            if d_mode['frame_capture'] == True and capture_session == False:
                mode_skeleton_new = self.update_skeleton_dict(d_mode, mode_skeleton)
                target_detected = self.traget_detected
                checkpoint = self.checkpoint
                facedetect = self.facedetect
                prevac = self.prevac
                proc_detection = Process(target=faces.detect.init_detection, args=(interval, window, d_mode,
                                                                                   d_detect, d_xmpp, d_lcd,
                                                                                   resh, resw, target_detected,
                                                                                   checkpoint, prevac, facedetect,
                                                                                   scale, camera_num, cascpath,
                                                                                   faces_path, movie, colors,
                                                                                   mode_skeleton_new))
                proc_detection.start()
                capture_session = True
            if not d_mode['frame_capture']:
                capture_session = False
                mode_skeleton['frame_capture'] = False
                mode_skeleton['video_mode'] = False
                mode_skeleton['facedetect_mode'] = False
                mode_skeleton['sign_tracking'] = False
                mode_skeleton['sign_phony'] = False
                mode_skeleton['sign_phony'] = False
                mode_skeleton['shared_vars_from_lcd'] = False
                mode_skeleton['shared_vars_from_xmpp'] = False
                d_mode = self.update_manager_dict(d_mode, mode_skeleton)

            # self.update_manager_dict(d_mode, mode_skeleton)
            time.sleep(2)

    def update_skeleton_dict(self, input_manager_dict, new_dict):
        """
        update the skeleton dictionary from the current
        values of a manager dictionary
        :param input_manager_dict: the manager dictionary from which the values are taken
        :param new_dict: the dictionary to update
        :return: the dictionary to update
        """
        dict_temp = new_dict
        for key, value in new_dict.items():
            dict_temp[key] = input_manager_dict[key]
        new_dict = dict_temp
        return new_dict

    def update_manager_dict(self, input_manager_dict, new_dict):
        """
        Little trick to force the update of the manager.dict
        :param input_manager_dict: the dictionary to update
        :param new_dict: a simple dictionary containing the values
        :return: the manager.dictionary
        """
        dict_temp = input_manager_dict
        for key, value in new_dict.items():
            dict_temp[key] = value
        input_manager_dict = dict_temp
        return input_manager_dict


if __name__ == '__main__':
    # Setup the command line arguments.
    r = Robot('../')
    r.launch('r1d2.ini')
