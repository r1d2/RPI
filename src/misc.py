#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#


import logging


class Share:
    def __init__(self):
        self.shared_vars_from_lcd = False
        self.shared_vars_from_xmpp = False
        self.video_mode = False
        self.follow_mode = False
        self.facedetect_mode = False
        self.mapping_mode = False
        self.frame_capture = False
        self.sign_tracking = False
        self.sign_phony = False

    def set_vars(self, shared_vars_from_lcd=False, shared_vars_from_xmpp=False, frame_capture=False, video_mode=False, follow_mode=False, mapping_mode=False,
                 facedetect_mode=False, sign_tracking=False, sign_phony=False):
        self.frame_capture = frame_capture
        self.video_mode = video_mode
        self.follow_mode = follow_mode
        self.mapping_mode = mapping_mode
        self.facedetect_mode = facedetect_mode
        self.sign_tracking = sign_tracking
        self.sign_phony = sign_phony
        self.shared_vars_from_lcd = shared_vars_from_lcd
        self.shared_vars_from_xmpp = shared_vars_from_xmpp
        
    def set_listening_vars(self):
        self.shared_vars_from_lcd = False
        self.shared_vars_from_xmpp = False


class Colors:
    def __init__(self):
        # Colors
        self.red = (255, 0, 0)
        self.blue = (0, 0, 255)
        self.orange = (255, 155, 0)
        self.green = (0, 255, 0)
        self.yellow = (255, 255, 0)


class Misc:
    # def __init__(self, loglevel, n):
    #     self.loglevel = loglevel
    #     # self.setup_custom_logger(n)

    def setup_custom_logger(self, name, level):
        formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

        handler = logging.StreamHandler()
        handler.setFormatter(formatter)

        logger = logging.getLogger(name)
        # change DEBUG
        options = {"CRITICAL": self.critical,
                   "ERROR": self.error,
                   "WARNING": self.warning,
                   "INFO": self.info,
                   "DEBUG": self.deb,
                   "NOTSET": self.noset
                   }
        ll = logging.DEBUG
        try:
            ll = options[level]()
        except KeyError as e:
            print('KeyError - reason "%s"' % str(e), "unknown")

        logger.setLevel(ll)
        logger.addHandler(handler)
        return logger

    @staticmethod
    def critical():
        return logging.CRITICAL

    @staticmethod
    def error():
        return logging.ERROR

    @staticmethod
    def warning():
        return logging.WARNING

    @staticmethod
    def info():
        return logging.INFO

    @staticmethod
    def deb():
        return logging.DEBUG

    @staticmethod
    def noset():
        return logging.NOSET
