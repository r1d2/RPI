#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#
# This script is inspired by the work of Frédéric Le Roy in GNULinux magazine N° 166, 12/2013
# """
import cv2
import numpy as np
import time
import os
import sys

if sys.version_info < (3, 0):
    from sleekxmpp.util.misc_ops import setdefaultencoding
    setdefaultencoding('utf8')
else:
    raw_input = input

TRAINSET_FACE = "../cascades/cascades-available/lbpcascade_frontalface.xml"
IMAGE_SIZE = 170
NUMBER_OF_CAPTURE = 20
THRESHOLD = 30.0

# haarcascade_frontalface_alt.xml
# haarcascade_frontalface_alt2.xml
# haarcascade_frontalface_alt_tree.xml
# haarcascade_frontalface_default.xml
# haarcascade_eye.xml
# haarcascade_eye_tree_eyeglasses.xml
# haarcascade_mcs_eyepair_small.xml
# haarcascade_mcs_eyepair_big.xml

DOWNSCALE = 4


class Recognize:
    def __init__(self, image_path, interval=0.1):
        self.image_path = image_path
        self.identities = []
        self.images = []  # X
        self.images_index = []  # Y
        self.camera = None
        self.classifier_face = None
        self.classifier_eyes = None
        self.interval = interval
        self.model = None

    def prepare_camera(self):
        self.camera = cv2.VideoCapture(0)
        self.classifier_face = cv2.CascadeClassifier(TRAINSET_FACE)


    # print "OK prepare"
    def is_camera_available(self):
        if self.camera.isOpened():
            rval, frame = self.camera.read()
            #            print "OK opened"
            return rval
        else:
            print("camera not available", self.camera.isOpened())
            rval = False
            return False

    def get_faces(self, frame):
        minisize = (frame.shape[1] / DOWNSCALE, frame.shape[0] / DOWNSCALE)
        #        print frame.shape
        miniframe = cv2.resize(frame, minisize)
        faces = self.classifier_face.detectMultiScale(miniframe)
        return faces


    def capture(self, mode, callback=None, view=True):
        if view:
            cv2.namedWindow("preview")
        #        print "OK preview"
        i = 0
        capture_num = 1
        captures = []
        resized = None
        rval = True
        while rval:
            time.sleep(self.interval)
            rval, frame = self.camera.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
            frame_gray  = clahe.apply(frame_gray)
            faces = self.get_faces(frame)
            for f in faces:
                x, y, w, h = [v * DOWNSCALE for v in f]
                cv2.rectangle(frame, (x, y), (x + w, y + h), 255, 0, 0)
                x_min = x+ (w*0.15)
                x_max = x+(w*0.85)
                y_min = y+(0.05*h)
                y_max = y  + (0.9*h)
                resized = self.extract_and_resize(frame, int(x_min), int(y_min), int(x_max), int(y_max))

                if mode == 'identify':
                    #                    print "identity"
                    identity, confidence = self.identity(resized)
                    cv2.putText(frame, "%s (%s)" % (identity, confidence), (x, y),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0))
                    callback(identity, confidence)
            cv2.putText(frame, "Press [Esc] to quit", (5, 25),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0))
            if mode == 'aquire':
                cv2.putText(frame, "Press [c] to collect", (5, 50),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0))
            if view:
                cv2.imshow("preview", frame)
            key = cv2.waitKey(20)
            if mode == 'aquire':
                if key == 1048675:  # collect on c
                    if resized is not None:
                        blurry = True
                        blurry = self.blurry_detection(resized)
                        if not blurry:
                            captures.append(resized)
                            cv2.imshow("Capture", resized)
                            capture_num += 1
                        if capture_num >= NUMBER_OF_CAPTURE:
                            return captures


            if key == 1048603:
                break
            #            else:
            #                print key # else print its value

    def train(self):
        #        self.model = cv2.createFisherFaceRecognizer()
        self.model = cv2.createLBPHFaceRecognizer()
        self.model.train(np.asarray(self.images),
                         np.asarray(self.images_index))


    def identity(self, image):
        [p_index, p_confidence] = self.model.predict(image)
        found_identity = self.identities[p_index]
        return found_identity, p_confidence


    def recognize(self, callback=None, view=True):
        self.read_images()  #rename as collection??
        self.train()
        self.prepare_camera()
        if not self.is_camera_available():
            return
        self.capture(mode='identify', callback=callback, view = True)


    def acquire(self, identity):
        self.prepare_camera()
        if not self.is_camera_available():
            return
        captures = self.capture(mode='aquire')
        self.add_to_collection(identity, captures)


    def extract_and_resize(self, frame, x_min, y_min, x_max, y_max):
        #cropped = cv2.getRectSubPix(frame, (w, h), (x + w / 2, y + h / 2))
        cropped = frame[y_min:y_max, x_min:x_max]
        grayscale = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        grayscale  = clahe.apply(grayscale)
        resized = cv2.resize(grayscale, (IMAGE_SIZE, IMAGE_SIZE))
        return resized


    def add_to_collection(self, identity, images):
        os.makedirs("{0}/{1}".format(self.image_path, identity))
        idx = 1

        for img in images:
            cv2.imwrite("{0}/{1}/{2}.jpg".format(self.image_path,
                                                 identity, idx), img)
            idx += 1


    def read_images(self, sz=None):
        #   """Reads the images in a given folder, resizes images on the fly
        #    if size is given.
        # Args:
        #        path: Path to a folder with subfolders representing the subjects (persons).
        #        sz: A tuple with the size Resizes

        c = 0
        self.images = []
        self.images_index = []
        for dirname, dirnames, filenames in os.walk(self.image_path):
            for subdirname in dirnames:
                self.identities.append(subdirname)
                #                print self.identities, subdirname
                subject_path = os.path.join(dirname, subdirname)
                for filename in os.listdir(subject_path):
                    try:
                        im = cv2.imread(os.path.join(subject_path,
                                                     filename), cv2.IMREAD_GRAYSCALE)
                        if sz is not None:
                            im = cv2.resize(im, sz)
                        self.images.append(np.asarray(im, dtype=np.uint8))
                        self.images_index.append(c)
                    except IOError as e:
                        print "I/O error({0}): {1}".format(e.errno, e.strerror)
                        raise
                c += 1
            #        print "OK read"


    def blurry_detection(self, resized):
        fm = self.variance_of_laplacian(resized)
        ret = True
        if fm > THRESHOLD:  # image is not blurry
            ret = False
            print('Not blurry: ', fm)
        return ret


    def variance_of_laplacian(self, image):
	    # compute the Laplacian of the image and then return the focus
	    # measure, which is simply the variance of the Laplacian
	    return cv2.Laplacian(image, cv2.CV_64F).var()



def display_recognize(identity, confidence):
    print("identity = {0} (confidence = {1})".format(identity, confidence))


if __name__ == "__main__":
    R = Recognize(image_path="../known_faces/", interval=0.1)
    print("Modes \n (1):acquire \n (2): recognize")
    mode = raw_input("Mode: ")
    if mode == "1":
        id = raw_input("Identity: ")
        R.acquire(identity = id)


    if mode =="2":
        R.recognize(callback=display_recognize, view=False)




# if C.is_camera_available():
#        captures = C.capture()
#        C.add_to_collection("arnaud", captures)
