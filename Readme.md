# WARNING

This project is abandoned. The codebase has been updated in the following [repository](https://gitlab.com/r1d3/rpi).

# Description
R1D2 is a project that aim to built a connected robot. It is based on a raspberry PI 2 and in the future an Arduino Mega will bring the motricity with the help of several motors.
The program is written in Python2 and the followings functionalities are already available. 

## V 0.1.x:
* Configuration though a `.ini` file.
* XMPP capabilities: 
  - connect to a server with a given jid.
  - connect to a multi user chat (MUC).
  - command the robot through AdHoc XMPP commands (see [here](http://xmpp.org/extensions/xep-0050.html)). Some examples of commands are:
        + change the mode of the robot (face detection, sign recognition, stop the OpenCV process, ...).   See the OpenCV capabilities.
        + send the status of the robot to a given jid.
        + disconnect
        + obtain the status of the robot (e.g. face detection, sign tracking, available) is given in the roster and the robot is marked as "busy"
  - ...
* OpenCV capabilities:
  - detect faces.
  - recognize faces after training
  - tell in the MUC "I am with ${recognized person} (confidence = [calculated confidence - the lower the better])"
  - detect a sign placed on a red panel (see [here](http://roboticssamy.blogspot.be/2013/11/rs4-robot-update-new-features.html)).
  - tell in the MUC that it found the sign.
  - ...

## Future versions
 The next version (V 0.2) will bring the following functionalities:
* Command the robot with a Char LCD plate (16x2) and a simple menu. It is connected by I2C (see [here](https://www.adafruit.com/products/1109))
* multiprocessing capabilities to isolate the several loops (menu, OpenCV and XMPP).
* Writing a video file of the capture

 The next versions will focus on:  
* Control motors
* Mapping
* Dialog with Arduino
* Enhance the face recognition procedure
* Python 3.0 and OpenCV 3.0 support
* ...



# Install and configuration
- You can install R1D2 by cloning the repository: `git clone https://gitlab.com/r1d2/RPI.git`.
- Then, you have to copy the file `r1d2.example.ini` to `r1d2.ini`.
- You have to edit it. The options are described.
- If you want the robot to use its OpenCV capabilities, you have to download some cascade files in XML format. You can found some of them in the following locations:
  + [fist](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/fist.xml)  
  + [palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/palm.xml)  
  + [closed frontal palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/closed_frontal_palm.xml)  
  + [lbpcascade frontalface](https://github.com/Itseez/opencv/blob/master/data/lbpcascades/lbpcascade_frontalface.xml)  

- Copy the XML files in the following directory `cascades/cascades-available`.
- Create a symbolic link in the directory `cascades/cascades-enabled` for each cascade file you want to 
activate. For example, if you want R1D2 to detect your fist:
```
cd cascades/cascades-enabled
ln -s ../cascades/cascades-available/fist.xml
```
If you want R1D2 to recognize you, you have to train it. 
- Connect your webcam to a computer equipped with a display. 
- Launch the script `build_faces_library.py` in the utils directory. You have to press `1` to acquire and give your name.
- Press 'c' when the script detect your face. By default, 20 pictures are taken.
- Afterwhat you can test the recognition procedure by relaunching `build_faces_library.py` and press `2`. Don't forget to enable the cascade file `lbpcascade_frontalface.xml`
- When your training data set is satisfying, you can launch R1D2 by typing `python2 robot.py` in the root directory of the project. R1D2 will enter a standby mode. For now, you have to use the AdHoc command in order to change the mode (i.e. detect your face).

If you want the robot to communicate with XMPP, you have to create an account for it on a server and set the jid in the `r1d2.ini` file as well as it password.
For now, the MUC is mandatory.

# Requirement
+ Sleekxmpp
+ OpenCV 2.4

# Licence
R1D2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

R1D2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
